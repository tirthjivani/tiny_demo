import React, { useRef } from 'react';
import { Editor } from '@tinymce/tinymce-react';
import axios from 'axios';

export default function TinyMceEditor() {
  const initialValue = '';

  const API_KEY = `${process.env.REACT_APP_API_KEY}`;
  // console.log(API_KEY);

  const editorRef = useRef(null);

  //start
  const setupWebComponent = (win, doc, editor) => {
    const template = doc.createElement('template');
    template.innerHTML = `
    <style>
       .container{
       width: 300px; 
       height: auto;
       font-family: 'Open Sans', sans-serif;
       font-weight:400;
       color:#53626E;
       border: #29539b 3px solid;
       padding: 20px 40px 10px 40px;
       @include box-sizing(border-box);
       border-radius:10px;
       background-color:#FFF;
       }
      
       button {
        margin:20px 0 30px 0;
        background-color: #29539b;
        background-image: linear-gradient(315deg, #29539b 0%, #1e3b70 74%);
        color:#FFF;
        border:0;
        width:100%; height: 40px;
        font-size:20px;
        border-radius:4px;
        
       }
       .Edit-btn{
        background-color: #29539b;
        background-image: linear-gradient(315deg, #29539b 0%, #1e3b70 74%);
        color:#FFF;
        border:0;
        width:25%; height: 20px;
        font-size:10px;
        border-radius:4px;
        cursor:pointer;
        
       }

       h3{
        margin:20px 0 10px 0;
       }

       ul{
        list-style:none;
        margin:0; padding:0;
       }

       li{
        list-style:none; 
        margin:0;
        
       }

       input{
       
       }
      .Option{
         display:flex;
         margin:20px 0px;
       }
    </style>
  <div class="container">
    <button class="Edit-btn" type="button" id="btn">EDIT POLL</button>
    <h3 id="title"></h3>
    <ul>
       <span class="Option">
      <input name="poll_value" type="radio" value="option 1" />
        <li id="option1" style=""></li>
        </span>
        <span class="Option">
        <input name="poll_value" type="radio" value="option 2" />
        <li id="option2"></li>
        </span>
        <span class="Option">
        <input name="poll_value" type="radio" value="option 3" />
        <li id="option3"></li>
        </span>
        <span class="Option">
        <input name="poll_value" type="radio" value="option 4" />
        <li id="option4"></li>
        </span>
      </ul>
      <button type="button" id="btn">SUBMIT</button>
    </div>`;

    class PollBlock extends win.HTMLElement {
      constructor() {
        super();
        this.setAttribute('contenteditable', false);
        const shadow = this.attachShadow({ mode: 'open' });
        this.shadowRoot.appendChild(template.content.cloneNode(true));
      }
      connectedCallback() {
        const cleanupContentEditable = () => {
          if (this.firstChild.contentEditable !== 'true') {
            const editableWrapper = document.createElement('div');
            editableWrapper.setAttribute('contenteditable', true);

            while (this.firstChild) {
              editableWrapper.appendChild(this.firstChild);
            }

            this.appendChild(editableWrapper);
          }
        };
        cleanupContentEditable();

        const editConditionalBlock = () => {
          dialogManager(this, editor, true);
          return false;
        };
        this.shadowRoot
          .getElementById('btn')
          .addEventListener('click', editConditionalBlock);
      }

      attributeChangedCallback(name, _oldValue, newValue) {
        if (name === 'data-title') {
          this.shadowRoot.getElementById('title').textContent = newValue;
        } else if (name === 'data-option1') {
          this.shadowRoot.getElementById('option1').textContent = newValue;
        } else if (name === 'data-option2') {
          this.shadowRoot.getElementById('option2').textContent = newValue;
        } else if (name === 'data-option3') {
          this.shadowRoot.getElementById('option3').textContent = newValue;
        } else if (name === 'data-option4') {
          this.shadowRoot.getElementById('option4').textContent = newValue;
        }
      }

      static get observedAttributes() {
        return [
          'data-title',
          'data-option1',
          'data-option2',
          'data-option3',
          'data-option4',
          'data-pollid',
        ];
      }
    }

    win.customElements.define('poll-container', PollBlock);
  };

  const dialogManager = (conditionalBlock, editor, isEdit) => {
    editor.windowManager.open({
      title: 'Enter Poll Details',
      body: {
        type: 'panel',
        items: [
          {
            type: 'input',
            name: 'title',
            label: 'Poll Question',
          },
          {
            type: 'input',
            name: 'option1',
            label: 'Poll Option 1',
          },
          {
            type: 'input',
            name: 'option2',
            label: 'Poll Option 2',
          },
          {
            type: 'input',
            name: 'option3',
            label: 'Poll Option 3',
          },
          {
            type: 'input',
            name: 'option4',
            label: 'Poll Option 4',
          },
        ],
      },
      buttons: [
        {
          type: 'cancel',
          name: 'closeButton',
          text: 'Cancel',
        },
        {
          type: 'submit',
          name: 'submitButton',
          text: 'Save',
          primary: true,
        },
      ],
      initialData: {
        title: conditionalBlock ? conditionalBlock.dataset.title : '',
        option1: conditionalBlock ? conditionalBlock.dataset.option1 : '',
        option2: conditionalBlock ? conditionalBlock.dataset.option2 : '',
        option3: conditionalBlock ? conditionalBlock.dataset.option3 : '',
        option4: conditionalBlock ? conditionalBlock.dataset.option4 : '',
      },

      onSubmit: (dialog) => {
        var data = dialog.getData();

        if (isEdit) {
          editor.insertContent(
            `<poll-container data-pollid="${data.pollid}" data-title="${data.title}" data-Option1="${data.option1}" data-Option2="${data.option2}" data-Option3="${data.option3}" data-Option4="${data.option4}" />`,
          );
        }

        const polldata = {
          title: data.title,
          'option 1': data.option1,
          'option 2': data.option2,
          'opiton 3': data.option3,
          'option 4': data.option4,
        };

        axios
          .post('http://65.1.38.29:5000/create', polldata)
          .then((response) => console.log(response.data))
          .catch((error) => {
            console.error('There was an error!', error);
          });

        if (!conditionalBlock) {
          editor.insertContent(
            `<poll-container data-pollid="${data.pollid}" data-title="${data.title}" data-Option1="${data.option1}" data-Option2="${data.option2}" data-Option3="${data.option3}" data-Option4="${data.option4}" />`,
          );
        } else {
          editor.undoManager.transact(() => {
            conditionalBlock.dataset.title = data.title;
            conditionalBlock.dataset.option1 = data.option1;
            conditionalBlock.dataset.option2 = data.option2;
            conditionalBlock.dataset.option3 = data.option3;
            conditionalBlock.dataset.option4 = data.option4;
          });

          editor.nodeChanged();
        }
        dialog.close();
      },
    });
  };

  return (
    <>
      <Editor
        apiKey={API_KEY}
        cloudChannel="5-dev"
        onInit={(_evt, editor) => (editorRef.current = editor)}
        initialValue={initialValue}
        inline={false}
        disabled={false}
        init={{
          theme: 'silver',
          height: 600,
          plugins: 'image imagetools code preview noneditable',
          toolbar: 'image | poll-custom | code | preview',
          menubar: false,
          automatic_uploads: true,
          convert_urls: false,
          images_upload_credentials: true,
          file_picker_types: 'image',
          image_advtab: true,
          images_upload_handler: function (blobInfo, success) {
            var data = {
              sFileName: blobInfo.blob().name,
              sContentType: blobInfo.blob().type,
            };
            console.log(data);
            success(
              `http://localhost:3000/${data.sContentType}/${data.sFileName}`,
            );
          },
          custom_elements: 'poll-container',
          setup: (editor) => {
            editor.on('PreInit', () => {
              const win = editor.getWin();
              const doc = editor.getDoc();
              setupWebComponent(win, doc, editor);
              editor.serializer.addNodeFilter('poll-container', (nodes) => {
                nodes.forEach((node) => {
                  if (!!node.attr('contenteditable')) {
                    node.attr('contenteditable', null);
                    node.firstChild.unwrap();
                  }
                });
              });
            });
            editor.ui.registry.addButton('poll-custom', {
              text: 'ADD POLL',
              tooltip: 'Add Poll',
              onAction: () => {
                dialogManager(null, editor, false);
              },
            });
          },
        }}
      />
    </>
  );
}
